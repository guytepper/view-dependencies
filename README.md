# [View Dependencies](https://view-deps.surge.sh/)

> View the dependency tree for an npm package.

## Build Setup

```bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn run dev

# run tests
yarn test
```
