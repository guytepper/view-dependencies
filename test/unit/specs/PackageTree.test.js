import PackageTree from '../../../src/components/PackageTree.vue';
import { mount, shallow } from 'vue-test-utils';
import Vue from 'vue';

const PackageTreeComponent = mount(PackageTree);

beforeAll(() => {
  window.fetch = jest.fn().mockImplementation(() =>
    Promise.resolve({
      json: () =>
        Promise.resolve({
          devDependencies: { nyc: '^10.0.0' },
          dependencies: { qs: '^6.0.2' }
        })
    })
  );

  PackageTreeComponent.setProps({ packageName: 'snyk' });
});

it('sets the dependency tree data correctly', () => {
  expect(PackageTreeComponent.vm.depsData).toEqual([
    { label: 'qs', version: '^6.0.2' },
    { label: 'nyc', version: '^10.0.0' }
  ]);
});
