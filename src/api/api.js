/**
 * Performs an HTTP get request.
 * @param {string} url - The url to perform the get request on.
 * @returns {Object} The response formatted as JSON.
 */
function request(url) {
  return fetch(url).then(res => res.json());
}

/**
 * Creates an npm API url endpoint for a package.
 * @param {string} name - The package name.
 * @param {string} [version] - The package version number.
 * @returns {string} The npm url endpoint for the provided package name.
 */
function getNPMUrl(name, version) {
  let url = '';
  // Use a proxy server when opening the app in localhost, to avoid CORS issues
  if (process.env.NODE_ENV !== 'production') {
    url = `https://registry.npmjs.org/${name}/`;
  } else {
    url = `https://cors-anywhere.herokuapp.com/https://registry.npmjs.org/${name}/`;
  }
  if (!version) url += 'latest';
  else url += version.replace(/[^\d\.]/gi, ''); // Replace semversion characters
  return url;
}

/**
 * Creates an object containing all of the package dependencies.
 * @param {Object} npmPackage - An object representing the package info.
 * @returns {Object} Object contains all of the package dependencies.
 */
function getDependencies(npmPackage) {
  let packages = {};
  if (npmPackage['dependencies']) packages = Object.assign({}, npmPackage['dependencies']);
  if (npmPackage['devDependencies']) packages = Object.assign(packages, npmPackage['devDependencies']);
  return packages;
}

/**
 *
 * @param {string} name - The pacakge name.
 * @param {string} [version] - The pacakge version.
 *
 */
async function getPackage(name, version) {
  const url = getNPMUrl(name, version);
  const response = await request(url);

  const deps = getDependencies(response);
  return deps;
}

const api = { getPackage };

export default api;
