import App from '../../../src/App.vue';
import { shallow } from 'vue-test-utils';
import Vue from 'vue';

const AppComponent = shallow(App);

it('updates the package name', () => {
  const packageName = 'snyk';
  AppComponent.vm.updatePackageName(packageName);
  expect(AppComponent.vm.packageName).toEqual(packageName);
});
